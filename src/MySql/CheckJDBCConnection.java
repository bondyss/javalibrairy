package MySql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CheckJDBCConnection 
{
	/**
	 * Important : in order to use JDBC object connection a copy of the mysql-connector-java-'editionNumber'-bin.jar
	 * is needed in a lib folder and the class path set
	 */
	public static void main(String[] args)
	{
		System.out.println("-------- MySQL JDBC Connection Testing ------------");
		 
		try
		{
			//check if the JDBC driver is available 
			Class.forName("com.mysql.jdbc.Driver");
		} 
		catch (ClassNotFoundException e) 
		{
			//if driver not found
			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();
			return;
		}
		
		//if the driver is found
		System.out.println("MySQL JDBC Driver Registered!");
		Connection connection = null;
	 
		try 
		{
			//establish a connection with the database
			connection = DriverManager
	                       //driver:databaseType://hostname:portnumber/database name, user, password				
			.getConnection("jdbc:mysql://localhost:3306/BudgetManagerDb","root", "007qr2mn8p");
	 
		} 
		catch (SQLException e) 
		{
			//catch connection issue
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
	 
		if (connection != null) 
		{
			System.out.println("You made it, take control your database now!");
		} 
		else 
		{
			System.out.println("Failed to make connection!");
		}
	}

}
