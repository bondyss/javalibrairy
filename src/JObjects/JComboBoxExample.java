package JObjects;
import javax.swing.JComboBox;
import javax.swing.JFrame;


public class JComboBoxExample extends JFrame
{
	private JFrame frame;
	
	public JComboBoxExample()
	{
		frame = new JFrame("JComboBox Example");
		createJComboBox();
		frame .setVisible(true);
	}
	public static void main(String[] args)
	{
		JComboBoxExample example = new JComboBoxExample();
	}
	private void createJComboBox()
	{
		//create a list
		String[] list = {"one","two","tree","four","five"};
		
		//create the comboBox of type String and initialize it with the list
		JComboBox<String> comboBox = new JComboBox(list);
		comboBox.setMaximumRowCount(3);//set the number of lines that will be displayed on the screen
		frame.add(comboBox);
		frame.pack();
	}
}
