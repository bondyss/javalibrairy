package JObjects;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class JOptionPanesAndJDialog extends JFrame
{
	private static JFrame frame;
	
	public static void main(String[] args)
	{
		frame = new JFrame("JOptionFrame demonstration");
		frame.setSize(250,250);
		frame.setVisible(true);
		//test dialogue box here
		simpleOptionDialogBox();
	}
	/**
	 * For most simple modal dialogs use 'JOptionPane.showMessageDialog'
	 * and for internal dialogs 'JOptionPane.showInternalMessageDialog'
	 * */
	private static void simpleMessageDialogueBox()
	{
		//frame to put the message into , message
		JOptionPane.showMessageDialog(frame, "Display message of your choice");
	}
	private static void simpleOptionDialogBox()
	{
		//constructor : ParentFrame, some question, some title, type of option, type of icon, Icon icon, Objects[], Objects Value
		int i = JOptionPane.showOptionDialog(frame,"Are you sure ?", "Some Title", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null, null, null);
		System.out.println(i);
	}
	private static void createJDialog(JFrame frame)
	{
		//create dialog frame
		System.out.println(frame);
		JDialog dialog = new JDialog(frame); //'frame' represent its parent window 
		dialog.setModal(true);
		dialog.setTitle("Input Dialog");
		
		
		dialog.setLayout(new BorderLayout());
		
		//create a input panel and button panel
		JPanel inputPanel = new JPanel(new FlowLayout(FlowLayout.LEADING,15,25));
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER,15,15));
		
		//create component to add to panels
		JLabel labelenterSomething = new JLabel("Enter something");
		JTextField textFieldEnterSomthing = new JTextField(15);
		JButton buttonOk = new JButton("OK");
		JButton buttonCancel = new JButton("Cancel");
		
		
		//add component to the panel
		inputPanel.add(labelenterSomething);
		inputPanel.add(textFieldEnterSomthing);		
		buttonPanel.add(buttonOk);
		buttonPanel.add(buttonCancel);
		
		//add the panel to the dialog frame
		dialog.add(inputPanel,BorderLayout.NORTH);
		dialog.add(buttonPanel,BorderLayout.CENTER);
		dialog.pack();
		dialog.setVisible(true);
	}
}	
