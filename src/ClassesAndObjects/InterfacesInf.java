package ClassesAndObjects;

public class InterfacesInf
{
	/**
	 * An interface is an object that define a method or a set of methods 
	 * to be implemented by one or many different objects.
	 * It is a reference type, similar to a class, that can contain only constants,
	 * method signatures, default methods, static methods, and nested types.
	 * Method bodies exist only for default methods and static methods. 
	 * Interfaces cannot be instantiated�they can only be implemented by classes or extended by other interfaces.	 
	 */
	//defining an interface
	public interface MethodsToBeImplemented
	{
		//write constants here if any...
		int aConstants = 0;
		
		//methods signatures...		
		//return type, methodName, braces and semicolon 		
		void method1();
		
		int method2(int parameterToRecive);
		//Etc...
	}
	/**
	 *A class that implements an interface must define ALL of it's methods  	 	
	 */
	//class to be implemented	
	public class ClassThatImplement implements MethodsToBeImplemented
	{
		public void method1()
		{
			//write methods body
		}
		public int method2(int expectAnInt)
		{
			//write method body
			expectAnInt = 0;
			return expectAnInt;
		}
	}
}
