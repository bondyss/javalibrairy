package ClassesAndObjects;

public class SubClassesInf 
{
	/**
	 * Subclasses are classes that inherit functionality of there superclass 
	 * They must define all of the abstract method of the superclass if any
	 * A subclass is also a type of superClass and all type of the hierarchy above 
	 */
	
	//declare the subclass
	//Accessibility, type, subclass name, keyword extends and superclass name 
	public class SubClass extends AbstractClassesInf
	{
		//declare ONLY the new variable to be used by the subclass
		private String newString;
		
		/**		 
		 * You have to write the parameters that the superclass constructor take plus the new ones
		 * (aString from superclass, newString from subclass) 
		 * because constructors are not inherited		 
		 */
		//declare constructor	
		public SubClass(String aString, String newString)
		{
			//the very first thing to do in a subclass constructor is to call the superclass constructor's
			super(aString);
			
			//then the new elements
			this.newString = newString;
		}
		
		//must define the abstract method of the superclass if any
		public void abstractMethod()
		{
			//write method body
		}
	}
}
