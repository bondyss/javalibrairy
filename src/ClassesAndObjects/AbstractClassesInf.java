package ClassesAndObjects;

public abstract class AbstractClassesInf 
{
	/**
	 * Abstract Classes can be used only as superclass.
	 * Others classes extends from the superclass to share a common design.
	 * They cannot be instantiated because they are incomplete
	 * Abstract superclass are too general to create objects  
	 * Concrete subclasses must implements the "missing pieces" 
	 */
		
	/**
	 * Abstract class can contain abstract method(s) witch will have to be defined in 
	 * the subclasses of the superclass
	 * They can also contains concrete method(s) witch are define in the abstract Class 
	 * Constructors and static methods cannot be declared abstract
	 * Constructors are not inherited 
	 * Non-private static methods are inherited but cannot be overridden
	 */
	//declare variables as normal
	private String aString;
	
	//declare a constructor as normal
	public AbstractClassesInf(String aString)
	{
		this.aString = aString;
	}		
	//abstract method declaration
	//accessibility, key word abstract, return type, method name, braces and semicolon
	public abstract void abstractMethod(); //body is to be implemented
	
	//concrete method declaration 
	//accessibility, return type, method name, braces, brackets and body
	public void concreteMethod()
	{
		//write method body here 
	}
	
}
