package ClassesAndObjects;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class NestedAndInnerClass
{
	/**
	 * Nested classes can be further classified into two different types of classes:
	 * non-static nested classes and static nested classes. 
	 * Non-static nested classes are more formally known as inner classes.
	 * The main thing to remember about inner classes(non-static nested) is that an instance of an inner class
	 * has access to all of the members of the outer class, even those that are marked �private�.
	 * The reason an inner class can�t be static is because of the fact that it�s impossible 
	 * to access non-static variables and methods from within a static context.
	 * So, a static inner class would only have access to the static members of the outer class.
	 */
	
	//here is an inner class
	public class ClassInsidAnOther
	{
		//class body
	}
	
	//here is an static nested class
	public static class StaticClassInsidAnOther
	{
		//class body
	}
	
	//instantiate an inner class from an enclosing method
	private void createComponents()
	{
		JButton myButton = new JButton("Button");
		myButton.addActionListener(new ActionListener()
		{				
	         public void actionPerformed(ActionEvent e) // enclosed method
	         {
	        	 //InnerClass      ObjName      new OutherClass().      new InnerClass();  
	        	 ClassInsidAnOther classIn = new NestedAndInnerClass(). new ClassInsidAnOther();
	         }
		});  
	}
}
