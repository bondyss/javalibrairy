package ClassesAndObjects;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

public class AnonymousClasses 
{
	/**
	 * Anonymous classes allow to declare and instantiate a class at the same time. 
	 * They are like local classes except that they do not have a name. 
	 * To use if you need to use a local class only once.
	 */
	private void anonymousExemple()
	{
		JLabel myLabel = new JLabel("Label");
		
							//implement a base class without giving it a name.
		myLabel.addMouseListener(new MouseAdapter()
		{				
			@Override //override the method
			public void mouseClicked(MouseEvent evt)
			{					
				//method body
			}						
		});						
	}
}
