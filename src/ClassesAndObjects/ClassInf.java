package ClassesAndObjects;

/**
* A class is a subclass of the class object 
*/	

//declare a class
//Accessibility, keyword class, class name 
public class ClassInf // implicitly extends object
{
	/**
	 * In most cases variables of classes should be declared private for security and robustness
	 * then access and changed by get and set methods
	 */
	//declare a variable
	//Accessibility, type, variable name, semicolon 
	private String aString; 
	
	//declare a constructor
	//Accessibility, class name, braces
	public ClassInf(String aString)	// take as parameters variables the constructor need to create the object
	{
		//Assign the variables 
		this.aString = aString;
	}
	
	//write method(s)
	//Accessibility, return type, method name, braces, parameters if any
	public void setAString(String aString)
	{
		this.aString = aString;
	}
	public String getAString()
	{
		return this.aString;
	}
	
	//good practice to override the toString methods of the superclass object
	@Override
	public String toString()
	{
		return String.format("Value of aString is : %s", getAString());	//better not use the variable name directly
	}															        //the get methods is better
}
